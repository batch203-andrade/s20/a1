let getNumberOfIterations = Number(prompt('Enter range of output: '));

function printIterations(range) {
    console.log('The number you provided is ' + range);
    for (let i = range; i >= 1; i -= 5) {
        if (i % 10 === 0) {
            if (i === 50) {
                console.log('The current value is at 50. Terminating the loop.');
                break;
            }
            console.log('The number is divisible by 10. Skipping the number');
            continue;
        }
        else if (i % 5 === 0) {
            console.log(i);
        }
    }
}

printIterations(getNumberOfIterations);


let string = 'supercalifragilisticexpialidocious';
function removeVowels(word) {
    let newWord = '';
    for (let i = 0; i < word.length; i++) {
        if (word[i].toLowerCase() === 'a' ||
            word[i].toLowerCase() === 'e' ||
            word[i].toLowerCase() === 'i' ||
            word[i].toLowerCase() === 'o' ||
            word[i].toLowerCase() === 'u') {
            continue;
        } else {
            newWord += word[i];
        }
    }

    return newWord;
}
console.log(string);
console.log(removeVowels(string));

